# Application setup

This project was built using HTML, CSS/SCSS, and Javascript

### `npm install`

Navigate to the project folder and run command
This will install the node-sass package. The node-sass enable the use of SCSS in the application

## Running the Application

### Option 1: VSCode Go live

Click the `Go Live` option at the bottom right of you VSCode. VSCode will start the live serve and run the application.

### Option 2: Liver Server Install

With the project terminal run `npx live-server`.
The Command will install liver-server if not installed and then run the application. If liver server is install, it will go ahead and run the application

Note: Due to the approach i used, the application cannot be started just opening the the indext.html file. Befor the application to take that appraoch, i will do the following: (see below `Alternative Approach`)

## Development Environment

If you used option 2 to run the application. Then open another terminal, otherwise open a terminal and run the command `npm run scss`. This command will listen for changes in any `.scss` within the `assets/styles` folder and compile it to `css` and write it into the `index.css` file

Note: For any .scss file to complie it must be within the `assets/styles` folder and the file must be added to the import section at the top of the `assets/styles/index.scss` file with using only the name of the file `(do not include the extention i.e .scss)`

## Assumptions

1. Beause no api was provided to get data, i created a set of dummy data within the `api` folder.
2. NO design was provided for the hover effect for the book cover within the recent book and all books section, only the design for that of featured books was provided, so i replicated the design as seen for the featured books on the recent and all books section respectively.

## Others

1. I used the HTML template element for building the component that needed to be used multiple times or that rquired data based on user interaction e.g is the the book-card. Another approach to handle this is to manully create the different element that is required to create the components at run time.
2. I used the the Flickity library to for the carousel

# Alternative Approach

If you try opening the application by running the index.html file, the applications wont work as expected, this is due to CORS issues.
To make the application work that way

1. The data within the api folders will be moved into the `books.js` file and intializing each as a variable so there won't be any need for a fetch request. Doing so will make the file to clumsy.
2. I will remove the `async` on the various functions as it will no longer be required
3. The data varaiable within the `allbooks`, `recentAddedBooks` and `featuredBooks` will be updated to match `2.`.

The reason for the approach i took is to make the application more organized and easy to read.
