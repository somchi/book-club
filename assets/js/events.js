let isOpen = false;
let searchIsActive = false;
let prevCard = '';
let value = '';
let filteredBooks = [];
let suggestions = [];

function toggleSidebar() {
  const open = !isOpen;
  isOpen = open;
  const sidebar = document.querySelector('aside');
  sidebar.setAttribute('data-show', isOpen);
}

function toggleSearch() {
  const isActive = !searchIsActive;
  searchIsActive = isActive;
  const form = document.querySelector('form');
  form.setAttribute('data-active', searchIsActive);
}

function openOverlay(ele, e) {
  e.stopPropagation();
  const element = document.getElementById(ele);
  if (prevCard !== '') {
    closeOverlay(prevCard);
    prevCard = ele;
  } else {
    prevCard = ele;
  }
  element.setAttribute('data-overlay', 'true');
}

function closeOverlay(ele) {
  const element = document.getElementById(ele);
  element.setAttribute('data-overlay', 'false');

  if (prevCard !== '') {
    const element = document.getElementById(prevCard);
    element.setAttribute('data-overlay', 'false');
    prevCard = '';
  }
}

function handleChange(val) {
  value = val;
  handleFilter(value);
}
