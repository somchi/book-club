const isSupported = 'content' in document.createElement('template');
const Menu = [
  { name: 'Home' },
  { name: 'Profile' },
  { name: 'Notification', count: 3 },
];

const SiteNav = [
  {
    section: 'Explore',
    menu: [
      { name: 'Books', count: 273 },
      { name: 'Genres', count: 42 },
      { name: 'Authors', count: 106 },
    ],
  },
  {
    section: 'My Books',
    menu: [
      { name: 'Queued', count: 3 },
      { name: 'Currently Borrowed', count: 0 },
      { name: 'Favourites', count: 19 },
      { name: 'History' },
    ],
  },
  {
    section: 'Admin',
    menu: [
      { name: 'Book Requests', count: 2 },
      { name: 'Members', count: 34 },
      { name: 'Libaray Settings' },
    ],
  },
];

function mainNav() {
  const mainNav = document.querySelector('.main-nav');
  if (isSupported) {
    const template = document.querySelector('#sidebar-item');

    Menu.forEach((element) => {
      const clone = template.content.cloneNode(true);
      const title = clone.querySelector('.menu-name');
      title.textContent = element.name;
      element.name === 'Home' && title.setAttribute('data-active', true);
      if (element.count) {
        const count = clone.querySelector('.menu-count');
        count.textContent = element.count;
        count.setAttribute('data-active', true);
      }
      mainNav.appendChild(clone);
    });
  } else {
  }
}

function otherNav() {
  const content = document.querySelector('.other-nav');
  const separator = document.createElement('div');
  separator.setAttribute('class', 'separator');

  if (isSupported) {
    const template = document.querySelector('#sidebar-item-other');
    const itemTemplate = document.querySelector('#sidebar-item');

    SiteNav.forEach((element) => {
      const clone = template.content.cloneNode(true);
      const sectionTitle = clone.querySelector('.section-title');
      sectionTitle.textContent = element.section;

      element.menu.forEach((item) => {
        const itemClone = itemTemplate.content.cloneNode(true);
        const title = itemClone.querySelector('.menu-name');
        title.textContent = item.name;

        if (item.count || item.count === 0) {
          const count = itemClone.querySelector('.menu-count');

          count.textContent = item.count;
          item.name === 'Book Requests' &&
            count.setAttribute('data-active', true);
        }
        clone.children[0].appendChild(itemClone);
      });
      content.appendChild(separator);
      content.appendChild(clone);
    });
  } else {
    const wrapper = document
      .createElement('div')
      .setAttribute('class', 'sidebar-section');
  }
}

mainNav();
otherNav();
