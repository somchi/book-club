export const Menu = [
  { name: 'Home' },
  { name: 'Profile' },
  { name: 'Notification', count: 3 },
];

export const SiteNav = [
  {
    section: 'Explore',
    menu: [
      { name: 'Books', count: 273 },
      { name: 'Genres', count: 42 },
      { name: 'Authors', count: 106 },
    ],
  },
  {
    section: 'My Books',
    menu: [
      { name: 'Queued', count: 3 },
      { name: 'Currently Borrowed', count: 0 },
      { name: 'Favourites', count: 19 },
      { name: 'History' },
    ],
  },
  {
    section: 'Admin',
    menu: [
      { name: 'Book Requests', count: 2 },
      { name: 'Members', count: 34 },
      { name: 'Libaray Settings' },
    ],
  },
];
