const Recent = async () => {
  const res = await fetch('api/recent.json');
  return res;
};
const AllBooks = async () => {
  const res = await fetch('api/data.json');
  return await res.json();
};
const FeaturedBooks = async () => {
  const res = await fetch('api/featured.json');
  return res;
};

const recentAddedBooks = async () => {
  const recentBooks = document.querySelector('.recent-books');
  if (isSupported) {
    const template = document.querySelector('#book-card');
    const res = await Recent();
    const data = await res.json();
    data.forEach((element) => {
      const clone = template.content.cloneNode(true);
      const cover = clone.querySelector('.book-cover');
      cover.setAttribute('src', element.thumbnail);
      const availability = clone.querySelectorAll('.book-status');
      availability[0].textContent = element.status;
      availability[1].textContent = element.status;
      element.status === 'Available' && setAvailability(availability);
      const name = clone.querySelectorAll('.book-name');
      name[0].textContent = element.name;
      name[1].textContent = element.name;
      const author = clone.querySelectorAll('.book-author');
      author[0].textContent = element.author;
      author[1].textContent = element.author;
      const genre = clone.querySelectorAll('.book-genre');
      genre[0].textContent = element.genre;
      genre[1].textContent = element.genre;
      const rate = clone.querySelectorAll('.rate');
      rate[0].textContent = element.rating;
      rate[1].textContent = element.rating;
      const numOfUser = clone.querySelectorAll('.num-users');
      numOfUser[0].textContent = element.viewed;
      numOfUser[1].textContent = element.viewed;
      const numOfReactions = clone.querySelectorAll('.num-reactions');
      numOfReactions[0].textContent = element.liked;
      numOfReactions[1].textContent = element.liked;
      recentBooks.appendChild(clone);
    });
  } else {
  }
};

const setAvailability = (availability) => {
  availability[0].setAttribute('data-availability', true);
  availability[1].setAttribute('data-availability', true);
};

const allBooks = async () => {
  const recentBooks = document.querySelector('.all-books');
  if (isSupported) {
    const template = document.querySelector('#book-card');
    const res = await AllBooks();

    const data = filteredBooks.length === 0 ? res : filteredBooks;

    data.forEach((element) => {
      const clone = template.content.cloneNode(true);
      const cover = clone.querySelector('.book-cover');
      cover.setAttribute('src', element.thumbnail);
      const availability = clone.querySelectorAll('.book-status');
      availability[0].textContent = element.status;
      availability[1].textContent = element.status;
      element.status === 'Available' && setAvailability(availability);
      const name = clone.querySelectorAll('.book-name');
      name[0].textContent = element.name;
      name[1].textContent = element.name;
      const author = clone.querySelectorAll('.book-author');
      author[0].textContent = element.author;
      author[1].textContent = element.author;
      const genre = clone.querySelectorAll('.book-genre');
      genre[0].textContent = element.genre;
      genre[1].textContent = element.genre;
      const rate = clone.querySelectorAll('.rate');
      rate[0].textContent = element.rating;
      rate[1].textContent = element.rating;
      const numOfUser = clone.querySelectorAll('.num-users');
      numOfUser[0].textContent = element.viewed;
      numOfUser[1].textContent = element.viewed;
      const numOfReactions = clone.querySelectorAll('.num-reactions');
      numOfReactions[0].textContent = element.liked;
      numOfReactions[1].textContent = element.liked;
      recentBooks.appendChild(clone);
    });
  } else {
  }
};

const featuredBooks = async () => {
  const books = document.querySelector('.featured-books');
  if (isSupported) {
    const template = document.querySelector('#featured-card');
    const res = await FeaturedBooks();
    const data = await res.json();
    data.forEach((element) => {
      const clone = template.content.cloneNode(true);
      const cover = clone.querySelector('.cover');
      cover.setAttribute('src', element.thumbnail);

      const bookId = clone.querySelector('.featured-item');
      bookId.setAttribute('id', element.id);

      const availability = clone.querySelector('.book-status');
      availability.textContent = element.status;

      element.status === 'Available' &&
        availability.setAttribute('data-availability', true);
      const name = clone.querySelector('.book-name');
      name.textContent = element.name;

      const author = clone.querySelector('.book-author');
      author.textContent = element.author;

      const genre = clone.querySelector('.book-genre');
      genre.textContent = element.genre;
      const lables = clone.querySelector('.book-labels');
      lables.textContent = element.labels;

      const rate = clone.querySelector('.rate');
      rate.textContent = element.rating;

      const numOfUser = clone.querySelector('.num-users');
      numOfUser.textContent = element.viewed;

      const numOfReactions = clone.querySelector('.num-reactions');
      numOfReactions.textContent = element.liked;

      const dots = clone.querySelector('.dots');
      dots.addEventListener('click', function (e) {
        openOverlay(element.id, e);
      });

      clone.children[0].addEventListener('click', function () {
        closeOverlay(element.id);
      });

      books.appendChild(clone);
    });
  }
};

const handleFilter = async (val) => {
  const books = await AllBooks();
  const filterData = books.filter((item) =>
    item.name.toLowerCase().includes(val.toLowerCase())
  );
  suggestions = filterData.map((item) => item.name);
  filteredBooks = filterData;
  const all = document.querySelector('.all-books');
  handleSuggestion(suggestions, val);
  all.innerHTML = '';
  if (filterData.length !== 0) {
    allBooks();
  }
  if (filteredBooks.length === 0 && value !== '') {
    const notFound = document.createElement('span');
    notFound.textContent = 'No Result';
    all.appendChild(notFound);
  }
};

function handleSuggestion(suggestions, val) {
  const box = document.querySelector('.suggestion');
  const status = box.getAttribute('data-show');
  box.innerHTML = '';
  if (val !== '') {
    if (status === 'false') {
      box.setAttribute('data-show', true);
    }
    if (suggestions.length !== 0) {
      suggestions.forEach((element) => {
        const suggestion = document.createElement('p');
        suggestion.textContent = element;
        suggestion.addEventListener('click', function () {
          value = element;
          handleFilter(element);
          box.setAttribute('data-show', false);
          const input = document.getElementById('search-field');
          input.value = element;
        });
        box.appendChild(suggestion);
      });
    } else {
      const notFound = document.createElement('p');
      notFound.textContent = 'No Result';
      box.appendChild(notFound);
    }
  } else {
    box.setAttribute('data-show', false);
  }
}

featuredBooks();
recentAddedBooks();
allBooks();
